require 'byebug'

def array_sum(arr)
  arr.reduce(&:+).to_i
end

def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| string.include?(substring) }
end

def non_unique_letters(string)
  string.delete!(" ")
  repeated_chars = []
  chars = string.split("").sort
  chars.each_with_index do |char, i|
    next if char != chars[i+1]
    repeated_chars << char
  end
  repeated_chars.uniq
end

def longest_two_words(string)
  string.delete!("!.,:;\"\'?")
  string.split(" ").sort_by { |word| word.length}[-2..-1]
end

def missing_letters(string)
  missing_arr = []
  [*("a".."z")].each do |char|
    next if string.include?(char)
    missing_arr << char
  end
  missing_arr
end

def all_uniq_nums?(num)
  num_chars = num.to_s.chars
  num_chars == num_chars.uniq
end

def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| all_uniq_nums?(year)}
end


def one_week_wonders(songs)
  uniq_songs = songs.uniq
  uniq_songs.select { |song| no_repeats?(song, songs)}
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, i|
    if song == song_name
      return false if songs[i+1] == song
    end
  end
  true
end


def for_cs_sake(string)
  string.delete!(".,!?;:")
  c_words = string.split.select {|word| word.include?("c")}
  c_words.sort_by {|word| c_distance(word)}.first
end

def c_distance(word)
  word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  start_index = nil
  arr.each_with_index do |num, i|
    next_num = arr[i+1]
    if num == next_num
      start_index = i unless start_index
    elsif start_index
      ranges << [start_index, i]
      start_index = nil
    end
  end
  ranges

end
