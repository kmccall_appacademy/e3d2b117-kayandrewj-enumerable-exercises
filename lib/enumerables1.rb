def get_evens(arr)
  arr.select { |el| el%2 == 0}
end

def calculate_doubles(arr)
  arr.map { |num| num*2}
end

def calculate_doubles!(arr)
  arr.map! { |num| num*2}
end

def array_sum_with_index(arr)
  total = 0
  arr.each_with_index do |num, i|
    total += (num*i)
  end
  total
end

def price_is_right(bids, actual_retail_price)
  bids.select! {|bid| bid < actual_retail_price}
  bids.sort[-1]
end

def at_least_n_factors(numbers, n)
   numbers.select {|num| num_factors(num) >= n}
end

def num_factors(num)
  factor_arr = [1, num]
  (num-2).times do |i|
    i = i+2
    next if num%i != 0
    factor_arr << i
  end
  factor_arr.length
end

def ordered_vowel_words(words)
  words.select { |word| ordered_vowel_word?(word)}
end

def ordered_vowel_word?(word)
  chars = word.split("")
  chars.select! { |char| %{a e i o u}.include?(char)}
  chars.sort == chars
end

def products_except_me(numbers)
  products = []
  numbers.length.times do
    products << array_product(numbers[1..-1])
    numbers = numbers.rotate()
  end
  products
end

def array_product(array)
  array.reduce(&:*)
end
